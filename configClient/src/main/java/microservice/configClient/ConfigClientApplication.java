package microservice.configClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class ConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigClientApplication.class, args);
	}

}
@RefreshScope
@RestController
class MessageController {

	@Value("${message:Hello default}")
	private String message;
	
	@Value("${server.port}")
	private int port;

	@RequestMapping("/message")
	public String getMessage() {
		return message+Integer.toString(port);
	}
}


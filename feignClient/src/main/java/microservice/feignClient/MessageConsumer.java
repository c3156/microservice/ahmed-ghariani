package microservice.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("springClient")
public interface MessageConsumer {

    @RequestMapping("/message")
    String getMessage();
}

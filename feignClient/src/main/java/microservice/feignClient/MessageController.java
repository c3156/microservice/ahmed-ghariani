package microservice.feignClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Autowired
    MessageConsumer consumer;

    @RequestMapping("/message/**")
    String getMessage(){
        return consumer.getMessage()+" from FeignClient";
    }
}

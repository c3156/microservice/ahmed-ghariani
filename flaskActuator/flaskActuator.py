from flask import Flask
from flask_pyctuator.flask_pyctuator import FlaskPyctuator

app = Flask("Flask App with Pyctuator")


@app.route("/")
def hello():
    return "Hello World!"


FlaskPyctuator(
    app,
    registration_url="http://localhost:9090/instances",
    pyctuator_endpoint_url="http://127.0.0.1:5000/pyctuator",
)
if __name__ == "__main__":
    app.run(debug=False, port=5000, host="localhost")

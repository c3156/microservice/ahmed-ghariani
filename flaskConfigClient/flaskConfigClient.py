import requests
from flask import Flask, jsonify
import yaml


app = Flask(__name__)
config = yaml.load(open("config.yaml"), yaml.CLoader)
app.config.update(config)


def get_config():
    respond = requests.get(
        f"{app.config['spring']['config']['uri']}/{app.config['app_name']}/{app.config['profile']}/{app.config['label']}")
    json = {}
    if respond.status_code == 200:
        json = respond.json()
        properties = [i['source'] for i in json['propertySources'][::-1]]
        for p in properties:
            app.config.update(p)
    return json


get_config()


@app.route('/config/refresh', methods=['POST'])
def refresh_config():
    return get_config()


@app.route('/message')
def get_message():
    return jsonify(message=app.config.get('message', 'Hello default!'))


if __name__ == '__main__':
    app.run(port=app.config['port'])

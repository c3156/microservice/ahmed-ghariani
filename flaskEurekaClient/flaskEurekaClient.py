import socket

from flask import Flask ,request
import py_eureka_client.eureka_client as eureka_client
import yaml

app = Flask(__name__)
config = yaml.load(open("config.yaml"), yaml.CLoader)
app.config.update(config)
hostname = socket.gethostname()
app_name = app.config['app_name']
port = app.config['port']
eureka_client.init(
    app_name=app_name,
    instance_host=hostname,
    instance_id=f"{hostname}:{app_name}:{port}",
    instance_port=port
    )


@app.route('/info')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=app.config['port'])
